public class Monom          ///note to self: sometime add some damn comments
{
                            ///define some variables we are going to work with;
    int powerNum;           ///numerator of the power
    int powerDen;           ///denuminator of the power
    int varNum;             ///variable's numerator
    int varDen;             ///variable's denuminator


    Monom(int varn, int vard, int powern, int powerd)       ///we define what a monom is
    {
        if((powerd!=1)&&(powerd>0)&&(powern % powerd==0))            /// if we can divide the numerator and denumenator, we do so
        {
            powern=powern/powerd;                                    /// after we divide them, the denuminator will have to become 1 because is useless at this point
            powerd=1;
        }
        if((vard!=1)&&(vard>0)&&(varn % vard==0))       /// we do the same as we did with the power
        {
            varn=varn/vard;
            vard=1;
        }
        powerNum=powern;
        powerDen=powerd;
        varNum=varn;
        varDen=vard;
    }

    public String showMonom(Monom m)            ///a simple method used to display the Monom and help test whether the class works as intended
    {
        String string=Integer.toString(m.getVarN())+"/"+Integer.toString(getVarD())+"x^"+Integer.toString(m.getPowerN())+"/"+Integer.toString(m.getPowerD());
        return string;
    }
    public void setvar(int varN, int varD)  ///variable setter
    {
        varNum=varN;
        varDen=varD;
    }

    public void setpow(int powN, int powD)  ///power setter
    {
        powerNum=powN;
        powerDen=powD;
    }

    public int getVarN()            ///variable getters
    {
        return varNum;
    }

    public int getVarD()
    {
        return varDen;
    }

    public int getPowerN()          ///power getters
    {
        return powerNum;
    }

    public int getPowerD()
    {
        return powerDen;
    }

    public static void main(String[] args)
    {
        Monom monom= new Monom(6,2,12,5);
        System.out.print(monom.showMonom(monom));
    }
}
