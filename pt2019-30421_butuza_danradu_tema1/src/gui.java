import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
/**
 * Created by Admin on 02-Apr-18.
 */
/*public class guiimport java.awt.FlowLayout;
        import java.awt.event.ActionListener;
        import java.awt.event.ActionEvent;
        import javax.swing.*;
*/
public class gui extends JFrame {

    private JTextField item1;
    private JTextField item1D;
    private JTextField item2;
    private JTextField item2D;
    private JTextField item3;
    private JTextField item3D;
    private JTextField item4;
    private JTextField item4D;
    private JTextField item6;
    private JTextField item6D;
    private JTextField item7;
    private JTextField item7D;
    private JTextField item8;
    private JTextField item8D;
    private JTextField item9;
    private JTextField item9D;
    private JTextArea item5;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JFrame frame;
    private JPasswordField passwordField;

    public gui(){
        super("Polynomial Processer UwU");
        setLayout(new FlowLayout());

        item1 = new JTextField("PowNum1",6);
        item1.setEditable(false);
        add(item1);

        item2 = new JTextField(5);
        add(item2);

        item1D = new JTextField("PowDen1",6);
        item1D.setEditable(false);
        add(item1D);

        item2D = new JTextField(5);
        add(item2D);

        item3 = new JTextField("VarNum1", 5);
        item3.setEditable(false);
        add(item3);

        item4= new JTextField(5);
        add(item4);

        item3D = new JTextField("VarDen1", 5);
        item3D.setEditable(false);
        add(item3D);

        item4D= new JTextField(5);
        add(item4D);

        button1=new JButton("P1");
        add(button1);

        item6 = new JTextField("PowNum2",6);
        item6.setEditable(false);
        add(item6);

        item7 = new JTextField(5);
        add(item7);

        item6D = new JTextField("PowDen2",6);
        item6D.setEditable(false);
        add(item6D);

        item7D = new JTextField(5);
        add(item7D);

        item8 = new JTextField("VarNum2", 5);
        item8.setEditable(false);
        add(item8);

        item9= new JTextField(5);
        add(item9);

        item8D = new JTextField("VarDen2", 5);
        item8D.setEditable(false);
        add(item8D);

        item9D= new JTextField(5);
        add(item9D);

        button2=new JButton("P2");
        add(button2);

        button3=new JButton("+");
        add(button3);

        button4=new JButton("-");
        add(button4);

        button5=new JButton("*");
        add(button5);

        button6=new JButton("deriv");
        add(button6);

        button7=new JButton("Integrate");
        add(button7);

        item5= new JTextArea("",30,50);
        add(item5);
        /*passwordField = new JPasswordField("mypass");
        add(passwordField);*/

        thehandler handler = new thehandler();
        item1.addActionListener(handler);
        item2.addActionListener(handler);
        item3.addActionListener(handler);
        item4.addActionListener(handler);
        item1D.addActionListener(handler);
        item2D.addActionListener(handler);
        item3D.addActionListener(handler);
        item4D.addActionListener(handler);
        //item5.addActionListener(handler);
        item6.addActionListener(handler);
        item7.addActionListener(handler);
        item8.addActionListener(handler);
        item9.addActionListener(handler);
        item6D.addActionListener(handler);
        item7D.addActionListener(handler);
        item8D.addActionListener(handler);
        item9D.addActionListener(handler);
        button1.addActionListener(handler);
        button2.addActionListener(handler);
        button3.addActionListener(handler);
        button4.addActionListener(handler);
        button5.addActionListener(handler);
        button6.addActionListener(handler);
        button7.addActionListener(handler);
    }

    private class thehandler implements ActionListener {
        private int Grad1;
        private int Grad1D;
        private int Var1;
        private int Var1D;
        private int Grad2;
        private int Grad2D;
        private int Var2;
        private int Var2D;
        private Monom monom1;
        private Monom monom2;
        List<Monom> m1= new ArrayList<Monom>();
        List<Monom> m2= new ArrayList<Monom>();
        //Polynomial polinom2;
        public void actionPerformed(ActionEvent event){
            String string = "";
            int grad=0;
            int var=0;
            if(event.getSource()==item1)
                string=String.format("field 1: %s", event.getActionCommand());
            else if(event.getSource()==item2) {
                string = String.format("%s", event.getActionCommand());
                grad=Integer.parseInt(string);
                System.out.println(grad);
                Grad1=grad;
            }
            else if(event.getSource()==item3)
                string=String.format("field 3: %s", event.getActionCommand());
            else if(event.getSource()==item4) {
                string = String.format("%s", event.getActionCommand());
                var=Integer.parseInt(string);
                System.out.println(var);
                Var1=var;
            }
            else if(event.getSource()==item1D)
                string=String.format("field 1: %s", event.getActionCommand());
            else if(event.getSource()==item2D) {
                string = String.format("%s", event.getActionCommand());
                grad=Integer.parseInt(string);
                System.out.println(grad);
                Grad1D=grad;
            }
            else if(event.getSource()==item3D)
                string=String.format("field 3: %s", event.getActionCommand());
            else if(event.getSource()==item4D) {
                string = String.format("%s", event.getActionCommand());
                var=Integer.parseInt(string);
                System.out.println(var);
                Var1D=var;
            }
            else if(event.getSource()==item6)
                string=String.format("field 1: %s", event.getActionCommand());
            else if(event.getSource()==item7) {
                string = String.format("%s", event.getActionCommand());
                grad=Integer.parseInt(string);
                System.out.println(grad);
                Grad2=grad;
            }
            else if(event.getSource()==item8)
                string=String.format("field 3: %s", event.getActionCommand());
            else if(event.getSource()==item9) {
                string = String.format("%s", event.getActionCommand());
                var=Integer.parseInt(string);
                System.out.println(var);
                Var2=var;
            }
            else if(event.getSource()==item6D)
                string=String.format("field 1: %s", event.getActionCommand());
            else if(event.getSource()==item7D) {
                string = String.format("%s", event.getActionCommand());
                grad=Integer.parseInt(string);
                System.out.println(grad);
                Grad2D=grad;
            }
            else if(event.getSource()==item8D)
                string=String.format("field 3: %s", event.getActionCommand());
            else if(event.getSource()==item9D) {
                string = String.format("%s", event.getActionCommand());
                var=Integer.parseInt(string);
                System.out.println(var);
                Var2D=var;
            }
            //JOptionPane.showMessageDialog(null, string);
            /*if(event.getSource()==button1)
               System.out.println("hey");*/

            if(event.getSource()==button1)
            {
                System.out.println(Var1+"   "+Var1D+"     "+Grad1+"   "+Grad1D);
                monom1= new Monom(Var1,Var1D,Grad1,Grad1D);
                m1=Polynom.AddToPolynom(m1,monom1);
                //m1=Polynomial.addMonoame(new MonomIntreg(5,5), new MonomIntreg(2,6));
                string=Polynom.showpoly(m1)+"0";
                item5.append("Polynom1=");
                item5.append(string);
                item5.append("\n");
            }
            if(event.getSource()==button2)
            {
                System.out.println(Var2+"   "+Grad2);
                monom2= new Monom(Var2,Var2D,Grad2,Grad2D);
                m2=Polynom.AddToPolynom(m2,monom2);
                //m1=Polynomial.addMonoame(new MonomIntreg(5,5), new MonomIntreg(2,6));
                string=Polynom.showpoly(m2)+"0";
                item5.append("Polynom2=");
                item5.append(string);
                item5.append("\n");
            }
            if(event.getSource()==button3)
            {
                List<Monom> list= new ArrayList<Monom>();
                list=Polynom.addPolinoame(m1,m2);
                string=Polynom.showpoly(list)+"0";
                item5.append("Sum=");
                item5.append(string);
                item5.append("\n");
            }
            if(event.getSource()==button4)
            {
                List<Monom> list= new ArrayList<Monom>();
                list=Polynom.subPolinoame(m1,m2);
                string=Polynom.showpoly(list)+"0";
                item5.append("Subtraction=");
                item5.append(string);
                item5.append("\n");
            }
            if(event.getSource()==button5)
            {
                List<Monom> list= new ArrayList<Monom>();
                list=Polynom.MultiplyPoly(m1,m2);
                string=Polynom.showpoly(list)+"0";
                item5.append("Multiplication=");
                item5.append(string);
                item5.append("\n");
            }
            if(event.getSource()==button6)
            {
                List<Monom> list= new ArrayList<Monom>();
                list=Polynom.DerivatePolinom(m1);
                m1=list;
                string=Polynom.showpoly(list)+"0";
                item5.append("Derivative=");
                item5.append(string);
                item5.append("\n");
            }
            if(event.getSource()==button7)
            {
                List<Monom> list= new ArrayList<Monom>();
                list=Polynom.Integrate(m1);
                m1=list;
                string=Polynom.showpoly(list)+"0";
                item5.append("Derivative=");
                item5.append(string);
                item5.append("\n");
            }
            //item5.append("\n");
        }

    }
}

