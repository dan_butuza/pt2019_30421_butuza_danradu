/**
 * Created by Admin on 09-Mar-19.
 */
import jdk.internal.dynalink.MonomorphicCallSite;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

public class Polynom
{                                                       ///we are going to treat the polynoms as a list of monoms
    static List<Monom> m1= new ArrayList<Monom>();
    static List<Monom> m2= new ArrayList<Monom>();
    private int power;
    private int var;

    Polynom()
    {
        List<Monom> m= new ArrayList<Monom>();
    }

    public static Monom CreateMonom(int varn, int vard, int powern, int powerd)
    {
        Monom newmonom= new Monom(varn, vard, powern, powerd);
        return newmonom;
    }

    public static int compareMonoms(Monom m1, Monom m2)             ///method for comparing the monoms
    {
        if ((m1.getPowerN()==m2.getPowerN())&&(m1.getVarN()==m2.getVarN()))
        {
            return 0;   ///returns 0 if they are equal
        }
        else
        {
            if((m1.getPowerN()==m2.getPowerN())&&((m1.getVarN()<m2.getVarN())||(m2.getVarN()<m1.getVarN())))
                return 1;   ///returns 1 if they have different variables
            else
                return 2; ///returns 2 if they have nothing in common
        }
    }

    public static List<Monom> AddToPolynom(List<Monom> a, Monom monom)
    {
        List<Monom> list=new ArrayList<Monom>();    ///method for adding monoms to the polynom
        int ok=0;
        for(Monom m:a)
        {                                           //we compare out current monom with all other monoms in the polinom
            //list.add(m);
            if (compareMonoms(m,monom)==0)          //if they are equal then we double the variable of the given monom
            {
                list.add(new Monom(m.getVarN()*2,1, m.getPowerN(),1));
                ok=1;
            }
            else if(compareMonoms(m,monom)==1)      ///if they only have the same power, then add the variables
            {
                list.add(new Monom(m.getVarN()+monom.getVarN(),1,m.getPowerN(),1));
                ok=1;
            }

        }
        if(ok==0)       //if we encountered no other monom with the same power as ours, then just add it to the polynom
            a.add(monom);
        return a;
    }

    public static List<Monom> addPolinoame(List<Monom> p1, List<Monom> p2)  //method for adding polynoms
    {
        int[] Powarray= new int[100];           //we are going to need an occurance array that i called Powarray. Neat name, i know UwU
        List<Monom> list= new ArrayList<Monom>();
        int index1=0;
        int index2=0;

        List<Monom> p3= new ArrayList<Monom>();

        for(Monom m1:p1)
        {
            Powarray[m1.getPowerN()]=m1.getVarN();  //we store the variable of the monom on the position corresponding to the power of that monom
        }
        for (Monom m2:p2)
        {
            Powarray[m2.getPowerN()]=Powarray[m2.getPowerN()]+m2.getVarN(); //should they have the same power, the value on that position will change to the sum of the variables
        }
        for(int i=0; i<Powarray.length;i++)     //afterwards, we create the monoms using our Powarray and add them to a list that will represent the sum of the polynoms UwU
        {
            if(Powarray[i]!=0)
            {
                Monom m=new Monom(Powarray[i],1, i,1);
                list.add(m);
            }
        }
        return list;
    }

    public static List<Monom> subPolinoame(List<Monom> p1, List<Monom> p2)  //same drill as addition but this time, we use "-" instead of "+"
    {
        int[] Powarray= new int[100];
        List<Monom> list= new ArrayList<Monom>();
        for(Monom m1:p1)
        {
            Powarray[m1.getPowerN()]=m1.getVarN();
        }
        for (Monom m2:p2)
        {
            Powarray[m2.getPowerN()]=Powarray[m2.getPowerN()]-m2.getVarN();
        }
        for(int i=0; i<Powarray.length;i++)
        {
            if(Powarray[i]!=0)
            {
                Monom m=new Monom(Powarray[i],1, i,1);
                list.add(m);
            }
        }
        return list;
    }

    public static List<Monom> DerivatePolinom(List<Monom> p)    //derivate a Polynom OwO
    {
        List<Monom> list = new ArrayList<Monom>();              //'tis quite elementary. We decrease the power by 1 and multiply the variable with the previous power
        for(Monom mono:p)
        {
            if(mono.getPowerN()>0)
            {
                Monom aux = new Monom(mono.getVarN() * mono.getPowerN(),1, mono.getPowerN() - 1,1);
                list.add(aux);
            }
            else if(mono.getPowerN()==0)
                list.add(new Monom(0,1,0,1));   //and if the power is 0, then we have not much left to derivate
        }                                                               //DERIVING A NUMBER EQUALS TO 0
        return list;
    }

    public static List<Monom> Integrate(List<Monom> m)      //I N T E G R A T E
    {                                                       // variable is divided by the (power+1)
        List<Monom> list=new ArrayList<Monom>();            // power gets incremented by 1
        for(Monom monom:m)
        {
            list.add(new Monom(monom.getVarN(),monom.getVarD()*(monom.getPowerN()+1),monom.getPowerN()+1,monom.getPowerD()));
        }
        return list;    //'tis mostly elementary
    }

    public static List<Monom> MultiplyPoly(List<Monom> m1, List<Monom> m2)
    {
        int[] result=new int[9999];         //M U L T I P L Y I N G the polynoms requires 3 arrays
        int[] monom1=new int[999];          //monom1 and monom2  hold the variable on the power position
        int[] monom2=new int [999];
        for(int i=0;i<9999;i++)
        {
            result[i]=0;
        }
        for(int i=0;i<999;i++)
        {
            monom1[i]=0;
            monom1[i]=0;
        }
        List<Monom> list= new ArrayList<Monom>();
        for(Monom mono: m1)
        {
            monom1[mono.getPowerN()]=mono.getVarN();
        }
        for(Monom mono:m2)
        {
            monom2[mono.getPowerN()]=mono.getVarN();
        }
        for(int i=0;i<monom1.length;i++)
        {
            for(int j=0;j<monom2.length;j++)
            {
                result[i+j]+=monom1[i]*monom2[j];   //the result will hold on the position of the added powers the multiplied values of the variables.
            }                                          // same drill as adding but we made it O(N^2) cus i was lazy >~>
        }
        for(int i=0; i<result.length;i++)
        {
            if(result[i]!=0)
            {
                Monom m=new Monom(result[i],1, i,1);
                list.add(m);
            }
        }
        return list;
    }


    public static String showpoly(List<Monom> a)        //method used for printing a polynom. Used extensively for the GUI
    {
        String string="";
        for(Monom monom: a)
        {
            string=string+"("+monom.getVarN()+"/"+monom.getVarD()+")"+"x^"+"("+monom.getPowerN()+"/"+monom.getPowerD()+")"+"+";
        }
        return string;
    }

}
